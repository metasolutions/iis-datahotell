#!/usr/bin/env bash

echo "Building EntryScape-Catalog edit application using require.js."
../node_modules/requirejs/bin/r.js -o profile.js mainConfigFile=../config/editConfig.js include=iis-datahotell/config/editConfig dir=../release/edit/
